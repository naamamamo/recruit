<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Candidates')->insert([
            'name' => Str::random(10),
            'email' => Str::random(5).'@gmail.com',
            'created_at'=>date('Y-m-d H:i:s'),   
            'updated_at'=>date('Y-m-d H:i:s'),    
        
        ]);
    }
}
